<?php

declare(strict_types=1);

define('ROOT_FOLDER', dirname(__DIR__) . '/');
define('APP_FOLDER', ROOT_FOLDER . 'src/');

require ROOT_FOLDER . 'vendor/autoload.php';

use Dotenv\Dotenv;
use Monolog\Logger;
use Paneric\DIContainer\DIContainer;
use Paneric\Local\Local;
use Paneric\Logger\LoggerFactory;
use Paneric\ModuleResolver\DefinitionsCollector;
use Paneric\ModuleResolver\ModuleResolver;
use Slim\App;

$dotEnv = Dotenv::createImmutable(ROOT_FOLDER);
$dotEnv->load();
define('ENV',$_ENV['ENV']);

if (ENV !== 'dev') {
    ini_set('display_errors', '1');
    ini_set('log_errors', '0');
    ini_set('error_log', ROOT_FOLDER . 'var/logs/server.log');
    error_reporting(E_ALL);

    error_log($_SERVER['REQUEST_URI'], 0);
}

try {
    $moduleResolverConfig = require APP_FOLDER . 'config/module-resolver-config.php';
    $moduleResolver = new ModuleResolver();
    $moduleFolder = $moduleResolver->setModuleFolderName(
        $_SERVER['REQUEST_URI'],
        $moduleResolverConfig
    );

    define('MODULE_FOLDER', $moduleFolder);
    define('MODULE_TEMPLATES_FOLDER', MODULE_FOLDER . 'templates/');

    $localValue = $moduleResolver->getLocal();

    $localConfig = require APP_FOLDER . 'config/local-config.php';
    $local = new Local();
    $localValue = $local->setValue($localConfig[ENV], $moduleResolverConfig['local_map'], $_COOKIE, $localValue);

    $definitionsCollector = new DefinitionsCollector();
    $definitions = $definitionsCollector->setDefinitions(
        APP_FOLDER,
        MODULE_FOLDER,
        'config',
        'en',
        ENV
    );

    $container = new DIContainer();
    $container->set('local', $localValue);
    $container->set('base_path', ROOT_FOLDER);
    $container->merge($definitions);

    $loggerFactory = new LoggerFactory([
        'path' => ROOT_FOLDER . 'var/logs',
        'level' => Logger::DEBUG,
        'file_permission' => 0777,
    ]);
    $loggerApp = $loggerFactory
        ->addFileHandler('app.log')
        ->addConsoleHandler()
        ->createInstance('app');
    $container->set(LoggerFactory::class, $loggerFactory);
    $container->set('logger_app', $loggerApp);

    $app = $container->get(App::class);

    require_once MODULE_FOLDER . 'bootstrap/middleware.php';
    require_once MODULE_FOLDER . 'bootstrap/routes-index.php';
    require_once MODULE_FOLDER . 'bootstrap/routes-cross.php';
    require_once MODULE_FOLDER . 'bootstrap/routes.php';

    $app->run();

} catch (Exception $e) {
    echo $e->getMessage();
}