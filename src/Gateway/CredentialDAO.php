<?php

declare(strict_types=1);

namespace ECommerce\Credential\Gateway;

use Paneric\DataObject\DAO;

class CredentialDAO extends DAO
{
    protected $id;
    protected $ref;//String 

    public function __construct()
    {
        $this->prefix = 'crd_';

        $this->setMaps();
    }

    /**
     * @return null|int|string
     */
    public function getId()
    {
        return $this->id;
    }
    public function getRef(): ?String
    {
        return $this->ref;
    } 

    /**
     * @var int|string
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    public function setRef(String $ref): void
    {
        $this->ref = $ref;
    } 

}
