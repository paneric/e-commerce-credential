<?php

declare(strict_types=1);

namespace ECommerce\Credential\Repository;

use Paneric\BaseModule\Module\Repository\ModuleRepository;

class CredentialRepository extends ModuleRepository implements CredentialRepositoryInterface
{}
