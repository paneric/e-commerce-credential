<?php

declare(strict_types=1);

namespace ECommerce\Credential\Repository;

use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;

interface CredentialRepositoryInterface extends ModuleRepositoryInterface
{}
