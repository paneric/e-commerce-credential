<?php

declare(strict_types=1);

return [
    'default_route_key' => 'crd',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => false,
    'module_map' => [
        'crd' => ROOT_FOLDER . 'src/CredentialApp',
        'api-crd' => ROOT_FOLDER . 'src/CredentialApi',
        'api-crds' => ROOT_FOLDER . 'src/CredentialApi',
        'apc-crd' => ROOT_FOLDER . 'src/CredentialApc',
    ],
    'module_map_cross' => [
        'crd'       => ROOT_FOLDER . 'vendor/paneric/e-commerce-credential/src/CredentialApp',
        'api-crd'   => ROOT_FOLDER . 'vendor/paneric/e-commerce-credential/src/CredentialApi',
        'api-crds'  => ROOT_FOLDER . 'vendor/paneric/e-commerce-credential/src/CredentialApi',
        'apc-crd'   => ROOT_FOLDER . 'vendor/paneric/e-commerce-credential/src/CredentialApc',
    ],
];
