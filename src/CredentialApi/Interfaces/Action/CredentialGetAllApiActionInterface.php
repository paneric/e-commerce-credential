<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApi\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\Api\GetAllApiActionInterface;

interface CredentialGetAllApiActionInterface extends GetAllApiActionInterface
{}
