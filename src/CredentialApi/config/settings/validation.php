<?php

declare(strict_types=1);

use ECommerce\Credential\Gateway\CredentialDTO;

return [

    'validation' => [

        'post.api-crd' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                    ], 

                ],
            ],
        ],

        'put.api-crd' => [
            'methods' => ['PUT'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                    ], 

                ],
            ],
        ],
    ]
];
