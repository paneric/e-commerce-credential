<?php

declare(strict_types=1);

use ECommerce\Credential\CredentialApi\CredentialApiController;

return [
    'credential_controller' => static function (): CredentialApiController {
        return new CredentialApiController();
    },
];
