<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Middleware\AuthenticationMiddleware;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Middleware\RouteMiddleware;
use Tuupola\Middleware\JwtAuthentication;

return [
    RouteMiddleware::class => static function (Container $container): RouteMiddleware {
        return new RouteMiddleware($container);
    },

    AuthenticationMiddleware::class => static function (Container $container): AuthenticationMiddleware {
        return new AuthenticationMiddleware($container->get(SessionInterface::class));
    },

    CSRFMiddleware::class => static function (Container $container): CSRFMiddleware {
        $config = $container->get('csrf');

        return new CSRFMiddleware(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $config
        );
    },

    PaginationMiddleware::class => static function (Container $container): PaginationMiddleware {
        return new PaginationMiddleware(
            $container
        );
    },

    JwtAuthentication::class => static function (Container $container):  JwtAuthentication {
        return new  JwtAuthentication(
            $container->get('jwt_authentication'),
        );
    },
];
