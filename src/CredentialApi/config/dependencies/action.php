<?php

declare(strict_types=1);

use ECommerce\Credential\Gateway\CredentialDAO;
use ECommerce\Credential\Gateway\CredentialDTO;

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Validation\ValidationService;

use ECommerce\Credential\CredentialApi\Action\CredentialAlterApiAction;
use ECommerce\Credential\CredentialApi\Action\CredentialCreateApiAction;
use ECommerce\Credential\CredentialApi\Action\CredentialDeleteApiAction;
use ECommerce\Credential\CredentialApi\Action\CredentialGetAllApiAction;
use ECommerce\Credential\CredentialApi\Action\CredentialGetAllPaginatedApiAction;
use ECommerce\Credential\CredentialApi\Action\CredentialGetOneByIdApiAction;

return [
    'credential_create_action' => static function (Container $container): CredentialCreateApiAction {
        return new CredentialCreateApiAction(
            $container->get('credential_repository'),
            $container->get(ValidationService::class),
            [
                'dao_class' => CredentialDAO::class,
                'dto_class' => CredentialDTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['crd_ref' => $attributes['ref']];
                },
            ]
        );
    },
    'credential_alter_action' => static function (Container $container): CredentialAlterApiAction {
        return new CredentialAlterApiAction(
            $container->get('credential_repository'),
            $container->get(ValidationService::class),
            [
                'dao_class' => CredentialDAO::class,
                'dto_class' => CredentialDTO::class,
                'update_unique_criteria' => static function (array $attributes, string $id): array
                {
                    return ['crd_id' => (int) $id, 'crd_ref' => $attributes['ref']];
                },
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['crd_id' => (int) $id];
                },
            ]
        );
    },
    'credential_delete_action' => static function (Container $container): CredentialDeleteApiAction {
        return new CredentialDeleteApiAction(
            $container->get('credential_repository'),
            [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['crd_id' => (int) $id];
                },
            ]
        );
    },
    'credential_get_all_action' => static function (Container $container): CredentialGetAllApiAction {
        return new CredentialGetAllApiAction(
            $container->get('credential_repository'),
            [
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    },
    'credential_get_all_paginated_action' => static function (Container $container): CredentialGetAllPaginatedApiAction {
        return new CredentialGetAllPaginatedApiAction(
            $container->get('credential_repository'),
            [
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    },
    'credential_get_one_by_id_action' => static function (Container $container): CredentialGetOneByIdApiAction {
        return new CredentialGetOneByIdApiAction(
            $container->get('credential_repository'),
            [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['crd_id' => (int) $id];
                },
            ]
        );
    },
];
