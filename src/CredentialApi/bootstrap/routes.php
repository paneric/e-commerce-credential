<?php

declare(strict_types=1);

use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\JwtAuthentication;

if (isset($app, $container)) {

    $app->get('/api-crds/{page}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->getAllPaginated(
            $request,
            $response,
            $this->get('credential_get_all_paginated_action'),
            $args['page']
        );
    })->setName('get.api-crds.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->get('/api-crds', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->getAll(
            $request,
            $response,
            $this->get('credential_get_all_action')
        );
    })->setName('get.api-crds')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->get('/api-crd/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->getOneById(
            $response,
            $this->get('credential_get_one_by_id_action'),
            $args['id']
        );
    })->setName('get.api-crd')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-crd', function (Request $request, Response $response) {
        return $this->get('credential_controller')->create(
            $request,
            $response,
            $this->get('credential_create_action')
        );
    })->setName('post.api-crd')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->put('/api-crd/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->alter(
            $request,
            $response,
            $this->get('credential_alter_action'),
            $args['id']
        );
    })->setName('put.api-crd')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->delete('/api-crd/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->delete(
            $request,
            $response,
            $this->get('credential_delete_action'),
            $args['id']
        );
    })->setName('delete.api-crd')
        ->addMiddleware($container->get(JwtAuthentication::class));
}
