<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApi\Action;

use ECommerce\Credential\CredentialApi\Interfaces\Action\CredentialGetOneByIdApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\GetOneByIdApiAction;

class CredentialGetOneByIdApiAction
    extends GetOneByIdApiAction
    implements CredentialGetOneByIdApiActionInterface
{}
