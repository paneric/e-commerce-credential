<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApi\Action;

use ECommerce\Credential\CredentialApi\Interfaces\Action\CredentialGetAllApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\GetAllApiAction;

class CredentialGetAllApiAction
    extends GetAllApiAction
    implements CredentialGetAllApiActionInterface
{}
