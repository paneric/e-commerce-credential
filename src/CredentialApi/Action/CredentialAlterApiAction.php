<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApi\Action;

use ECommerce\Credential\CredentialApi\Interfaces\Action\CredentialAlterApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\AlterApiAction;

class CredentialAlterApiAction
    extends AlterApiAction
    implements CredentialAlterApiActionInterface
{}
