<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApi\Action;

use ECommerce\Credential\CredentialApi\Interfaces\Action\CredentialDeleteApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\DeleteApiAction;

class CredentialDeleteApiAction
    extends DeleteApiAction
    implements CredentialDeleteApiActionInterface
{}
