<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApi\Action;

use ECommerce\Credential\CredentialApi\Interfaces\Action\CredentialGetAllPaginatedApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\GetAllPaginatedApiAction;

class CredentialGetAllPaginatedApiAction
    extends GetAllPaginatedApiAction
    implements CredentialGetAllPaginatedApiActionInterface
{}
