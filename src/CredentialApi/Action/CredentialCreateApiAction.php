<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApi\Action;

use ECommerce\Credential\CredentialApi\Interfaces\Action\CredentialCreateApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\CreateApiAction;

class CredentialCreateApiAction
    extends CreateApiAction
    implements CredentialCreateApiActionInterface
{}
