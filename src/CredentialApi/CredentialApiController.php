<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApi;

use Paneric\BaseModule\Module\Controller\ModuleApiController;

class CredentialApiController extends ModuleApiController {}
