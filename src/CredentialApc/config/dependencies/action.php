<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use ECommerce\Credential\CredentialApc\Action\CredentialAlterApcAction;
use ECommerce\Credential\CredentialApc\Action\CredentialCreateApcAction;
use ECommerce\Credential\CredentialApc\Action\CredentialDeleteApcAction;
use ECommerce\Credential\CredentialApc\Action\CredentialGetAllApcAction;
use ECommerce\Credential\CredentialApc\Action\CredentialGetAllPaginatedApcAction;
use ECommerce\Credential\CredentialApc\Action\CredentialGetOneByIdApcAction;

return [
    'credential_add_action' => static function (Container $container): CredentialCreateApcAction {
        return new CredentialCreateApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'credential',
                    'prefix' => 'crd'
                ]
            )
        );
    },
    'credential_edit_action' => static function (Container $container): CredentialAlterApcAction {
        return new CredentialAlterApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'credential',
                    'prefix' => 'crd'
                ]
            )
        );
    },
    'credential_remove_action' => static function (Container $container): CredentialDeleteApcAction {
        return new CredentialDeleteApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                ['module_name_sc' => 'credential']
            )
        );
    },
    'credential_show_all_action' => static function (Container $container): CredentialGetAllApcAction {
        return new CredentialGetAllApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'credential',
                    'prefix' => 'crd'
                ]
            )

        );
    },
    'credential_show_all_paginated_action' => static function (Container $container): CredentialGetAllPaginatedApcAction {
        return new CredentialGetAllPaginatedApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'credential',
                    'prefix' => 'crd'
                ]
            )

        );
    },
    'credential_show_one_by_id_action' => static function (Container $container): CredentialGetOneByIdApcAction {
        return new CredentialGetOneByIdApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'credential',
                    'prefix' => 'crd'
                ]
            )
        );
    },
];
