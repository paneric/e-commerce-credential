<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use ECommerce\Credential\CredentialApc\CredentialApcController;
use Twig\Environment as Twig;

return [
    'credential_controller' => static function (Container $container): CredentialApcController {
        return new CredentialApcController(
            $container->get(Twig::class),
            'apc-crd'
        );
    },
];
