<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc\Action;

use ECommerce\Credential\CredentialApc\Interfaces\Action\CredentialGetOneByIdApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\GetOneByIdApcAction;

class CredentialGetOneByIdApcAction
    extends GetOneByIdApcAction
    implements CredentialGetOneByIdApcActionInterface
{}
