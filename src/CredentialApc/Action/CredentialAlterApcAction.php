<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc\Action;

use ECommerce\Credential\CredentialApc\Interfaces\Action\CredentialAlterApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\AlterApcAction;

class CredentialAlterApcAction
    extends AlterApcAction
    implements CredentialAlterApcActionInterface
{}
