<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc\Action;

use ECommerce\Credential\CredentialApc\Interfaces\Action\CredentialDeleteApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\DeleteApcAction;

class CredentialDeleteApcAction
    extends DeleteApcAction
    implements CredentialDeleteApcActionInterface
{}
