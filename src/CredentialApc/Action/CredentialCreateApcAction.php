<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc\Action;

use ECommerce\Credential\CredentialApc\Interfaces\Action\CredentialCreateApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\CreateApcAction;

class CredentialCreateApcAction
    extends CreateApcAction
    implements CredentialCreateApcActionInterface
{}
