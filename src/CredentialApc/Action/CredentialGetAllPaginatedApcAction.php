<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc\Action;

use ECommerce\Credential\CredentialApc\Interfaces\Action\CredentialGetAllPaginatedApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\GetAllPaginatedApcAction;

class CredentialGetAllPaginatedApcAction
    extends GetAllPaginatedApcAction
    implements CredentialGetAllPaginatedApcActionInterface
{}
