<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc\Action;

use ECommerce\Credential\CredentialApc\Interfaces\Action\CredentialGetAllApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\GetAllApcAction;

class CredentialGetAllApcAction
    extends GetAllApcAction
    implements CredentialGetAllApcActionInterface
{}
