<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc;

use Paneric\BaseModule\Module\Controller\ModuleApcController;

class CredentialApcController extends ModuleApcController {}
