<?php

declare(strict_types=1);

use Paneric\Middleware\CSRFMiddleware;
use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {
    $app->get('/apc-crd/show-all', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->showAll(
            $request,
            $response,
            $this->get('credential_show_all_action')
        );
    })->setName('apc-crd.show_all')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->get('/apc-crd/show-one-by-id/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->showOneById(
            $request,
            $response,
            $this->get('credential_show_one_by_id_action'),
            $args['id']
        );
    })->setName('apc-crd.show_one_by_id')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-crd/add', function (Request $request, Response $response) {
        return $this->get('credential_controller')->add(
            $request,
            $response,
            $this->get('credential_add_action')
        );
    })->setName('apc-crd.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-crd/edit/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->edit(
            $request,
            $response,
            $this->get('credential_edit_action'),
            $this->get('credential_show_one_by_id_action')->getOneById($request, $args['id']),
            $args['id']
        );
    })->setName('apc-crd.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-crd/remove/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->remove(
            $request,
            $response,
            $this->get('credential_remove_action'),
            $args['id']
        );
    })->setName('apc-crd.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));
}
