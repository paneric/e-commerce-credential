<?php

declare(strict_types=1);

use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->get('/apc-crd/show-all-paginated[/{page}]', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->showAllPaginated(
            $request,
            $response,
            $this->get('credential_show_all_paginated_action'),
            empty($args) ? null : $args['page']
        );
    })->setName('apc-crd.show_all_paginated')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class)); // no pagination middleware !!!
}
