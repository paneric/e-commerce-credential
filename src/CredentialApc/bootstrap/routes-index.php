<?php

declare(strict_types=1);

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->get('/cms', function (Request $request, Response $response) {
        return $response;
    })->setName('cms.index');
}
