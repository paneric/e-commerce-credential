<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\Apc\GetAllActionInterface;

interface CredentialGetAllApcActionInterface extends GetAllActionInterface
{}
