<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApc\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\Apc\CreateActionInterface;

interface CredentialCreateApcActionInterface extends CreateActionInterface
{}
