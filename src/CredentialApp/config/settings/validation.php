<?php

declare(strict_types=1);

use ECommerce\Credential\Gateway\CredentialDTO;

return [

    'validation' => [

        'crd.add' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                    ], 

                ],
            ],
        ],

        'crd.edit' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [
                    'ref' => [
                    ], 

                ],
            ],
        ],

        'crd.remove' => [
            'methods' => ['POST'],
            CredentialDTO::class => [
                'rules' => [],
            ],
        ],
    ]
];
