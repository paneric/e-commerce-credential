<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Pagination\PaginationExtension;
use Paneric\Twig\Extension\CSRFExtension;
use Paneric\Twig\Extension\ValidationExtension;
use Paneric\Validation\Validator;

return [
    CSRFExtension::class => static function (Container $container): CSRFExtension
    {
        return new CSRFExtension(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $container->get('csrf')
        );
    },
    ValidationExtension::class => static function (Container $container): ValidationExtension
    {
        return new ValidationExtension(
            $container->get(Validator::class)
        );
    },
    PaginationExtension::class => static function (Container $container): PaginationExtension
    {
        return new PaginationExtension(
            $container->get(SessionInterface::class),
            $container->get('pagination-extension')
        );
    },
];
