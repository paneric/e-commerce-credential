<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use ECommerce\Credential\CredentialApp\CredentialAppController;
use Twig\Environment as Twig;

return [
    'credential_controller' => static function (Container $container): CredentialAppController {
        return new CredentialAppController(
            $container->get(Twig::class),
            'crd'
        );
    },
];
