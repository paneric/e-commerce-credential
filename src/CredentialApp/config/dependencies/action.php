<?php

declare(strict_types=1);

use ECommerce\Credential\Gateway\CredentialDAO;
use ECommerce\Credential\Gateway\CredentialDTO;

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Validation\ValidationService;

use ECommerce\Credential\CredentialApp\Action\CredentialAlterAppAction;
use ECommerce\Credential\CredentialApp\Action\CredentialCreateAppAction;
use ECommerce\Credential\CredentialApp\Action\CredentialDeleteAppAction;
use ECommerce\Credential\CredentialApp\Action\CredentialGetAllAppAction;
use ECommerce\Credential\CredentialApp\Action\CredentialGetAllPaginatedAppAction;
use ECommerce\Credential\CredentialApp\Action\CredentialGetOneByIdAppAction;

return [
    'credential_create_action' => static function (Container $container): CredentialCreateAppAction {
        return new CredentialCreateAppAction(
            $container->get('credential_repository'),
            $container->get(SessionInterface::class),
            $container->get(ValidationService::class),
            [
                'module_name_sc' => 'credential',
                'prefix' => 'crd',
                'dao_class' => CredentialDAO::class,
                'dto_class' => CredentialDTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['crd_ref' => $attributes['ref']];
                },
            ]
        );
    },
    'credential_alter_action' => static function (Container $container): CredentialAlterAppAction {
        return new CredentialAlterAppAction(
            $container->get('credential_repository'),
            $container->get(SessionInterface::class),
            $container->get(ValidationService::class),
            [
                'module_name_sc' => 'credential',
                'prefix' => 'crd',
                'dao_class' => CredentialDAO::class,
                'dto_class' => CredentialDTO::class,
                'update_unique_criteria' => static function (array $attributes, string $id): array
                {
                    return ['crd_id' => (int) $id, 'crd_ref' => $attributes['ref']];
                },
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['crd_id' => (int) $id];
                },
            ]
        );
    },
    'credential_delete_action' => static function (Container $container): CredentialDeleteAppAction {
        return new CredentialDeleteAppAction(
            $container->get('credential_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'credential',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['crd_id' => (int) $id];
                },
            ]
        );
    },
    'credential_get_all_action' => static function (Container $container): CredentialGetAllAppAction {
        return new CredentialGetAllAppAction(
            $container->get('credential_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'credential',
                'prefix' => 'crd',
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    },
    'credential_get_all_paginated_action' => static function (Container $container): CredentialGetAllPaginatedAppAction {
        return new CredentialGetAllPaginatedAppAction(
            $container->get('credential_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'credential',
                'prefix' => 'crd',
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    },
    'credential_get_one_by_id_action' => static function (Container $container): CredentialGetOneByIdAppAction {
        return new CredentialGetOneByIdAppAction(
            $container->get('credential_repository'),
            $container->get(SessionInterface::class),
            [
                'prefix' => 'crd',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['crd_id' => (int) $id];
                },
            ]
        );
    },
];
