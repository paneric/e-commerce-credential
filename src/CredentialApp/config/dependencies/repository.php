<?php

declare(strict_types=1);

use ECommerce\Credential\Gateway\CredentialDAO;
use ECommerce\Credential\Repository\CredentialRepository;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\DBAL\Manager;

return [
    'credential_repository' => static function (Container $container): CredentialRepository
    {
        return new CredentialRepository(
            $container->get(Manager::class),
            [
                'table' => 'credentials',
                'dao_class' => CredentialDAO::class,
                'create_unique_where' => sprintf(
                    ' %s',
                    'WHERE crd_ref=:crd_ref'
                ),
                'update_unique_where' => sprintf(
                    ' %s %s',
                    'WHERE crd_ref=:crd_ref',
                    'AND crd_id NOT IN (:crd_id)'
                ),
            ]
        );
    },
];
