<?php

declare(strict_types=1);

use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {
    $app->get('/crd/show-all', function (Request $request, Response $response) {
        return $this->get('credential_controller')->showAll(
            $response,
            $this->get('credential_get_all_action')
        );
    })->setName('crd.show_all');

    $app->get('/crd/show-one-by-id/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->showOneById(
            $response,
            $this->get('credential_get_one_by_id_action'),
            $args['id']
        );
    })->setName('crd.show_one_by_id');

    $app->map(['GET', 'POST'], '/crd/add', function (Request $request, Response $response) {
        return $this->get('credential_controller')->add(
            $request,
            $response,
            $this->get('credential_create_action')
        );
    })->setName('crd.add')
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/crd/edit/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->edit(
            $request,
            $response,
            $this->get('credential_alter_action'),
            $args['id']
        );
    })->setName('crd.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/crd/remove/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('credential_controller')->remove(
            $request,
            $response,
            $this->get('credential_delete_action'),
            $args['id']
        );
    })->setName('crd.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class));
}
