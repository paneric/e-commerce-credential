<?php

declare(strict_types=1);

use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->get('/crd/show-all-paginated[/{page}]', function (Request $request, Response $response) {
        return $this->get('credential_controller')->showAllPaginated(
            $request,
            $response,
            $this->get('credential_get_all_paginated_action')
        );
    })->setName('crd.show_all_paginated')
        ->addMiddleware($container->get(PaginationMiddleware::class));
}
