<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp;

use Paneric\BaseModule\Module\Controller\ModuleAppController;

class CredentialAppController extends ModuleAppController {}
