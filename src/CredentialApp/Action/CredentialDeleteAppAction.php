<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp\Action;

use ECommerce\Credential\CredentialApp\Interfaces\Action\CredentialDeleteAppActionInterface;
use Paneric\BaseModule\Module\Action\App\DeleteAppAction;

class CredentialDeleteAppAction
    extends DeleteAppAction
    implements CredentialDeleteAppActionInterface
{}
