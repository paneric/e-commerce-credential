<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp\Action;

use ECommerce\Credential\CredentialApp\Interfaces\Action\CredentialGetOneByIdAppActionInterface;
use Paneric\BaseModule\Module\Action\App\GetOneByIdAppAction;

class CredentialGetOneByIdAppAction
    extends GetOneByIdAppAction
    implements CredentialGetOneByIdAppActionInterface
{}
