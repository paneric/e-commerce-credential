<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp\Action;

use ECommerce\Credential\CredentialApp\Interfaces\Action\CredentialAlterAppActionInterface;
use Paneric\BaseModule\Module\Action\App\AlterAppAction;

class CredentialAlterAppAction
    extends AlterAppAction
    implements CredentialAlterAppActionInterface
{}
