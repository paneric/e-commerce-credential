<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp\Action;

use ECommerce\Credential\CredentialApp\Interfaces\Action\CredentialGetAllAppActionInterface;
use Paneric\BaseModule\Module\Action\App\GetAllAppAction;

class CredentialGetAllAppAction
    extends GetAllAppAction
    implements CredentialGetAllAppActionInterface
{}
