<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp\Action;

use ECommerce\Credential\CredentialApp\Interfaces\Action\CredentialCreateAppActionInterface;
use Paneric\BaseModule\Module\Action\App\CreateAppAction;

class CredentialCreateAppAction
    extends CreateAppAction
    implements CredentialCreateAppActionInterface
{}
