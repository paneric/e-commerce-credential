<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp\Action;

use ECommerce\Credential\CredentialApp\Interfaces\Action\CredentialGetAllPaginatedAppActionInterface;
use Paneric\BaseModule\Module\Action\App\GetAllPaginatedAppAction;

class CredentialGetAllPaginatedAppAction
    extends GetAllPaginatedAppAction
    implements CredentialGetAllPaginatedAppActionInterface
{}
