<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\App\AlterActionInterface;

interface CredentialAlterAppActionInterface extends AlterActionInterface
{}
