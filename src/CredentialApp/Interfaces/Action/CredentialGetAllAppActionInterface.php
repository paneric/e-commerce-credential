<?php

declare(strict_types=1);

namespace ECommerce\Credential\CredentialApp\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\App\GetAllActionInterface;

interface CredentialGetAllAppActionInterface extends GetAllActionInterface
{}
